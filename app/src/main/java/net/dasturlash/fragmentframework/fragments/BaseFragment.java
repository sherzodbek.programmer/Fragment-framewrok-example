package net.dasturlash.fragmentframework.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Sherzodbek on 30.09.2017. (ShopManager)
 */

public abstract class BaseFragment extends Fragment {
    private int resId;

    public BaseFragment(int resId) {
        super();
        this.resId = resId;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(resId, container, false);
        onInitView(view);
        onInitDefaultValue();
        return view;
    }

    public abstract void onInitView(View view);
    public abstract void onInitDefaultValue();
}
